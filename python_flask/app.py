from flask import Flask, json,jsonify,request
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema,fields

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://postgres:password@localhost:5432/flaskdb'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


class Employee(db.Model):
    id=db.Column(db.Integer(),primary_key=True)
    name=db.Column(db.String(255),nullable=False)
    designation=db.Column(db.String(255),nullable=False)

    def __repr__(self):
        return self.name
    
    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_by_id(cls,id):
        return cls.query.get_or_404(id)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class EmployeeSchema(Schema):
    id=fields.Integer()
    name=fields.String()
    designation=fields.String()

@app.route('/employees',methods=['GET'])
def get_all_employees():
    #db qyery
    employees=Employee.get_all()

    serializer=EmployeeSchema(many=True)
    data=serializer.dump(employees)
    print(data)
    print(type(data))
    return jsonify(data)

@app.route('/employees',methods=['POST'])
def create_a_employee():
    data=request.get_json()

    new_employee =Employee(
        name= data.get('name'),
        designation= data.get('designation')
    )
    new_employee.save()
    serializer=EmployeeSchema()
    data=serializer.dump(new_employee)
    return jsonify(
        data
    ),201

@app.route('/employee/<int:id>',methods=['GET'])
def get_employee(id):
    employee=Employee.get_by_id(id)

    serializer=EmployeeSchema()

    data=serializer.dump(employee)

    return jsonify(
        data
    ),200

@app.route('/employee/<int:id>',methods=['PUT'])
def update_employee(id):
    employee_to_update=Employee.get_by_id(id)

    data=request.get_json()

    employee_to_update.name=data.get('name')
    employee_to_update.designation=data.get('designation')

    db.session.commit()

    serializer=EmployeeSchema()

    employee_data=serializer.dump(employee_to_update)

    return jsonify(employee_data),200

@app.route('/employee/<int:id>',methods=['DELETE'])
def delete_employee(id):
    employee_to_delete=Employee.get_by_id(id)

    employee_to_delete.delete()

    return jsonify({"message":"Deleted"}),204    

@app.errorhandler(404)
def not_found(error):
    return jsonify({"message":"Resource not found on server"}),404

@app.errorhandler(500)
def internal_server(error):
    return jsonify({"message":"There is a problem at server-side"}),500



if __name__ == "__main__":
    app.run(debug=True)