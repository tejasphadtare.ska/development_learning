import unittest
import requests

class TestAPI(unittest.TestCase):
    url_get = "http://127.0.0.1:5000/project"
    post_data = {
    "Name" : "SKAO",
    "Status" : "PROGRESS",
    "NoOfUsers" : 20,
    "Size" : "LARGE",
    "Memory" : 155
    }
    def test_1(self):
        resp = requests.get(self.url_get)
        self.assertEqual(resp.status_code, 200)
        print("Test 1 for status code check for GET request has started")
        print("Test 1 completed")

    def test_2(self):
        resp = requests.get(self.url_get)
        self.assertEqual(len(resp.json()), 2)
        print("Test 2 for lenght of JSON data check has started")
        print("Test 2 completed")

    def test_3(self):
        resp = requests.post(self.url_get, json=self.post_data)
        self.assertEqual(resp.status_code, 201)
        print("Test 3 for status code check for POST request has started")
        print("Test 3 completed")

if __name__ == "__main__":
    tester = TestAPI()
    tester.test_1()
    tester.test_2()
    tester.test_3()