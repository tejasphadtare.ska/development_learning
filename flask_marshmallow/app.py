from flask import Flask, request
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields, validate, validates, ValidationError
import copy

app = Flask(__name__)
ma = Marshmallow(app)

project_status = ['CREATED', 'PROGRESS', 'CLOSED']
project_size = ['SMALL', 'MEDIUM', 'LARGE']

class ProjectResponseSchema(Schema):
    ProjectId = fields.Integer()
    Name = fields.Str()

class AddProjectRequestSchema(Schema):
    # field level validation
    Name = fields.Str(required=True, validate=validate.Length(min=3, max=255))
    Status = fields.Str(required=True, validate=validate.OneOf(project_status))
    NoOfUsers = fields.Integer(required=True)
    Size = fields.Str(required=True, validate=validate.OneOf(project_size))
    Memory = fields.Integer(required=True,validate=validate.Range(min=0,max=200))

    #custom validation
    @validates("NoOfUsers")
    def validate_NoOfUsers(self, value):
        if value < 10 or value > 20:
            raise ValidationError("NoOfUsers should be between 10 and 20")


@app.route('/project', methods=['GET', 'POST'])
def project():
    if request.method == 'GET':
        project_response_dict = {
            'ProjectId': 1,
            'Name': 'SKAO'
        }
        return ProjectResponseSchema().dump(project_response_dict)

    elif request.method == 'POST':
        errors = AddProjectRequestSchema().validate(request.json)
        
        if errors:
            return errors, 422

        # load will do both validation and loading
        project_request_dict = AddProjectRequestSchema().load(request.json)

        # Here we can load the data to Database
        return project_request_dict, 201


if __name__ == '__main__':
    app.run(debug=True)