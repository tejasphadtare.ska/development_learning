from dice import roll_dice


def guess_number(num):
    result = roll_dice()
    print(result)
    if result == num:
        return "You won!"
    else:
        return "You lost!"
