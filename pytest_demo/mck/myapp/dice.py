import random
from unittest import mock
def roll_dice():
    print("rolling...")
    return random.randint(1, 6)
print("Roll without mock")
print(roll_dice())

mock_roll_dice = mock.Mock(name="roll dice", return_value=3)

print("Roll with mock")
print(mock_roll_dice())

