from datetime import datetime

def add(a, b):
    return a + b

class Student:

    def __init__(self, name, dob):
        self.name = name
        self.dob = dob

    def get_age(self):
        return (datetime.now() - self.dob).days // 365
