from pytestpoc.myapp.sample import add, Student
import sys
import pytest
from datetime import datetime


# #simple assert
# def test_add_num():
#     assert add(1, 2) == 3

#pytest markers
#skip tests
@pytest.mark.skip(reason="to skip a test")
def test_add_num():
    assert add(1, 2) == 3

@pytest.mark.skipif(sys.version_info < (3, 9), reason="condition (use python 3.9 or greater)")
def test_add_str():
    print(sys.version_info)
    assert add("a", "b") == "ab"

#parameterize a test
@pytest.mark.parametrize("a,b,c", [(1, 2, 3), ("a", "b", "ab"),
                                   ([1, 2], [3], [1, 2, 3])],
                         ids=["num", "str", "list"])
def test_add(a, b, c):
    assert add(a, b) == c

#Fixtures
@pytest.fixture
def dummy_student():
    return Student("nikhil", datetime(2000, 1, 1))

def test_student_get_age(dummy_student):
    dummy_student_age = (datetime.now() - dummy_student.dob).days // 365
    assert dummy_student.get_age() == dummy_student_age